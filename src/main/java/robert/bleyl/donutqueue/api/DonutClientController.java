package robert.bleyl.donutqueue.api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import robert.bleyl.donutqueue.service.DonutOrderClientInfoDTO;
import robert.bleyl.donutqueue.service.DonutOrderService;

@RestController
@RequestMapping("/donut-orders")
public class DonutClientController {

	private DonutOrderService donutOrderService;

	@Autowired
	public DonutClientController(DonutOrderService donutOrderService) {
		this.donutOrderService = donutOrderService;
	}

	private boolean clientIdValid(int clientId) {
		return clientId >= DonutOrderService.MIN_CLIENT_ID && clientId <= DonutOrderService.MAX_CLIENT_ID;
	}

	private ResponseEntity<Object> createClientIdInvalidResponse(int clientId) {
		return ResponseEntity.badRequest().body("The clientId '%d' is invalid!".formatted(clientId));
	}

	@PostMapping("/{clientId}")
	public ResponseEntity<Object> addOrder(@PathVariable("clientId") int clientId, @RequestParam("quantity") int quantity) {
		if (!clientIdValid(clientId)) {
			return createClientIdInvalidResponse(clientId);
		}

		Optional<DonutOrderClientInfoDTO> orderInfoOpt = donutOrderService.addOrder(clientId, quantity);

		if (orderInfoOpt.isEmpty()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body("You already have an order in the queue!");
		}

		return ResponseEntity.ok(orderInfoOpt.get());
	}

	@GetMapping("/{clientId}")
	public ResponseEntity<Object> getOrderInfo(@PathVariable("clientId") int clientId) {
		if (!clientIdValid(clientId)) {
			return createClientIdInvalidResponse(clientId);
		}

		Optional<DonutOrderClientInfoDTO> orderInfoOpt = donutOrderService.getOrderInfo(clientId);

		if (orderInfoOpt.isEmpty()) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(orderInfoOpt.get());
	}

	@DeleteMapping("/{clientId}")
	public ResponseEntity<Object> cancelOrder(@PathVariable("clientId") int clientId) {
		if (!clientIdValid(clientId)) {
			return createClientIdInvalidResponse(clientId);
		}

		if (donutOrderService.cancelOrder(clientId)) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.notFound().build();
	}
}
