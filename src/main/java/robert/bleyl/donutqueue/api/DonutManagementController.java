package robert.bleyl.donutqueue.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import robert.bleyl.donutqueue.database.DonutOrderManagerInfoDTO;
import robert.bleyl.donutqueue.service.DonutOrderService;

@RestController
@RequestMapping("/donut-management")
public class DonutManagementController {

	private DonutOrderService donutOrderService;

	@Autowired
	public DonutManagementController(DonutOrderService donutOrderService) {
		this.donutOrderService = donutOrderService;
	}

	@GetMapping("")
	public List<DonutOrderManagerInfoDTO> getAllOrderInfos() {
		return donutOrderService.getAllOrderInfos();
	}
}