package robert.bleyl.donutqueue.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import robert.bleyl.donutqueue.database.DonutOrderCartInfoDTO;
import robert.bleyl.donutqueue.service.DonutOrderService;

@RestController
@RequestMapping("/donut-queue")
public class DonutQueueController {

	private DonutOrderService donutOrderService;

	@Autowired
	public DonutQueueController(DonutOrderService donutOrderService) {
		this.donutOrderService = donutOrderService;
	}

	@GetMapping("")
	public List<DonutOrderCartInfoDTO> getNextOrdersInQueue() {
		return donutOrderService.retrieveAndUpdateNextOrdersInQueue();
	}
}