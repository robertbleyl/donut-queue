package robert.bleyl.donutqueue;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.authorizeRequests()
				.antMatchers("/donut-orders/**").permitAll()
				.antMatchers("/donut-queue/**").hasRole("JIM")
				.antMatchers("/donut-management/**").hasRole("MANAGER")
				.anyRequest().authenticated()
				.and().httpBasic()
				.and().csrf().disable();
	}

	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		UserDetails jim = User.withUsername("jim")
				.passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode)
				.password("very$ecureJimPW").roles("JIM").build();

		UserDetails manager = User.withUsername("manager")
				.passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode)
				.password("very$ecureManagerPW").roles("MANAGER").build();

		InMemoryUserDetailsManager userDetailsManager = new InMemoryUserDetailsManager();
		userDetailsManager.createUser(jim);
		userDetailsManager.createUser(manager);
		return userDetailsManager;
	}
}
