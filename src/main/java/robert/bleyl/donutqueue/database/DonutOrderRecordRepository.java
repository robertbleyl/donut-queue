package robert.bleyl.donutqueue.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DonutOrderRecordRepository extends JpaRepository<DonutOrderRecord, Long> {

	Optional<DonutOrderRecord> findByClientId(int clientId);
}