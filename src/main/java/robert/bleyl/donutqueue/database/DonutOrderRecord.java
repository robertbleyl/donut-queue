package robert.bleyl.donutqueue.database;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "donut_order")
public class DonutOrderRecord {

	@Id
	@Column(name = "client_id")
	private int clientId;

	@Column(name = "creation_time_in_millis")
	private long creationTimeInMillis;

	private int quantity;

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public long getCreationTimeInMillis() {
		return creationTimeInMillis;
	}

	public void setCreationTimeInMillis(long creationTimeInMillis) {
		this.creationTimeInMillis = creationTimeInMillis;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}