package robert.bleyl.donutqueue.database;

import java.util.Objects;

public class DonutOrderCartInfoDTO {

	private int clientId;
	private int quantity;

	public DonutOrderCartInfoDTO() {

	}

	public DonutOrderCartInfoDTO(int clientId, int quantity) {
		this.clientId = clientId;
		this.quantity = quantity;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		return Objects.hash(clientId, quantity);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DonutOrderCartInfoDTO other = (DonutOrderCartInfoDTO)obj;
		return clientId == other.clientId && quantity == other.quantity;
	}
}