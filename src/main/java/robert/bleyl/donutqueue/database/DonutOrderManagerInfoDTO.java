package robert.bleyl.donutqueue.database;

import java.util.Objects;

public class DonutOrderManagerInfoDTO {

	private int clientId;
	private int quantity;
	private int queuePosition;
	private long waitTimeInMillies;

	public DonutOrderManagerInfoDTO() {

	}

	public DonutOrderManagerInfoDTO(int clientId, int quantity, int queuePosition, long waitTimeInMillies) {
		this.clientId = clientId;
		this.quantity = quantity;
		this.queuePosition = queuePosition;
		this.waitTimeInMillies = waitTimeInMillies;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getQueuePosition() {
		return queuePosition;
	}

	public void setQueuePosition(int queuePosition) {
		this.queuePosition = queuePosition;
	}

	public long getWaitTimeInMillies() {
		return waitTimeInMillies;
	}

	public void setWaitTimeInMillies(long waitTimeInMillies) {
		this.waitTimeInMillies = waitTimeInMillies;
	}

	@Override
	public int hashCode() {
		return Objects.hash(clientId, quantity, queuePosition, waitTimeInMillies);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DonutOrderManagerInfoDTO other = (DonutOrderManagerInfoDTO)obj;
		return clientId == other.clientId && quantity == other.quantity && queuePosition == other.queuePosition && waitTimeInMillies == other.waitTimeInMillies;
	}
}