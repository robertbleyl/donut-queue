package robert.bleyl.donutqueue.service;

import java.util.Objects;

public class DonutOrderClientInfoDTO {

	private int queuePosition;
	private long waitTimeInMillies;

	public DonutOrderClientInfoDTO() {

	}

	public DonutOrderClientInfoDTO(int queuePosition, long waitTimeInMillies) {
		this.queuePosition = queuePosition;
		this.waitTimeInMillies = waitTimeInMillies;
	}

	public int getQueuePosition() {
		return queuePosition;
	}

	public void setQueuePosition(int queuePosition) {
		this.queuePosition = queuePosition;
	}

	public long getWaitTimeInMillies() {
		return waitTimeInMillies;
	}

	public void setWaitTimeInMillies(long waitTimeInMillies) {
		this.waitTimeInMillies = waitTimeInMillies;
	}

	@Override
	public int hashCode() {
		return Objects.hash(queuePosition, waitTimeInMillies);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DonutOrderClientInfoDTO other = (DonutOrderClientInfoDTO)obj;
		return queuePosition == other.queuePosition && waitTimeInMillies == other.waitTimeInMillies;
	}
}