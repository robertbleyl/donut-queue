package robert.bleyl.donutqueue.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import robert.bleyl.donutqueue.database.DonutOrderCartInfoDTO;
import robert.bleyl.donutqueue.database.DonutOrderManagerInfoDTO;
import robert.bleyl.donutqueue.database.DonutOrderRecord;

public class DonutQueue {

	public static final long FIVE_MINUTES = 5 * 60 * 1000;
	private static final int CART_CAPACITY = 50;

	private final List<DonutOrderRecord> orders;
	private final List<DonutOrderRecord> toBeDeleted;
	private final List<DonutOrderManagerInfoDTO> dtos;

	private DonutOrderRecord toBeUpdated;

	private int queuePosition = 1;
	private int donutCountInCurrentCart;

	private boolean stopWhenFirstCartFull;
	private Optional<Integer> targetClientIdOpt = Optional.empty();

	public DonutQueue(List<DonutOrderRecord> orders) {
		this.orders = orders;

		toBeDeleted = new ArrayList<>(orders.size());
		dtos = new ArrayList<>(orders.size());
	}

	public List<DonutOrderManagerInfoDTO> getAllOrderInfos() {
		processRecords();
		return dtos;
	}

	public DonutQueueCartResult getOrderInfosForNextCart() {
		stopWhenFirstCartFull = true;

		processRecords();

		List<DonutOrderCartInfoDTO> cartDtos = dtos.stream().map(dto -> new DonutOrderCartInfoDTO(dto.getClientId(), dto.getQuantity())).toList();

		return new DonutQueueCartResult(cartDtos, toBeDeleted, Optional.ofNullable(toBeUpdated));
	}

	public Optional<DonutOrderClientInfoDTO> getOrderInfoForClient(int clientId) {
		targetClientIdOpt = Optional.of(clientId);

		processRecords();

		if (dtos.isEmpty()) {
			return Optional.empty();
		}

		DonutOrderManagerInfoDTO dto = dtos.get(0);
		DonutOrderClientInfoDTO clientDto = new DonutOrderClientInfoDTO(dto.getQueuePosition(), dto.getWaitTimeInMillies());
		return Optional.of(clientDto);
	}

	private void processRecords() {
		for (DonutOrderRecord record : orders) {
			int originalQuantity = record.getQuantity();
			int addedQuantity = tryAddingToCart(record);

			if (targetClientIdOpt.isEmpty() || targetClientIdOpt.get() == record.getClientId()) {
				addDto(record, addedQuantity, originalQuantity);

				if (targetClientIdOpt.isPresent()) {
					break;
				}
			}

			if (donutCountInCurrentCart == CART_CAPACITY) {
				if (stopWhenFirstCartFull) {
					break;
				}

				queuePosition++;
				donutCountInCurrentCart = 0;
			}
		}
	}

	private int tryAddingToCart(DonutOrderRecord record) {
		int possibleDonutsToBeAdded = CART_CAPACITY - donutCountInCurrentCart;

		if (record.getQuantity() > possibleDonutsToBeAdded) {
			donutCountInCurrentCart += possibleDonutsToBeAdded;
			record.setQuantity(record.getQuantity() - possibleDonutsToBeAdded);

			toBeUpdated = record;
			return possibleDonutsToBeAdded;
		}

		donutCountInCurrentCart += record.getQuantity();
		toBeDeleted.add(record);

		return record.getQuantity();
	}

	private void addDto(DonutOrderRecord record, int addedQuantity, int originalQuantity) {
		int quantity = stopWhenFirstCartFull ? addedQuantity : originalQuantity;
		long waitTime = queuePosition * FIVE_MINUTES;

		DonutOrderManagerInfoDTO dto = new DonutOrderManagerInfoDTO(record.getClientId(), quantity, queuePosition, waitTime);
		dtos.add(dto);
	}
}