package robert.bleyl.donutqueue.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import robert.bleyl.donutqueue.database.DonutOrderCartInfoDTO;
import robert.bleyl.donutqueue.database.DonutOrderManagerInfoDTO;
import robert.bleyl.donutqueue.database.DonutOrderRecord;
import robert.bleyl.donutqueue.database.DonutOrderRecordRepository;

@Service
public class DonutOrderService {

	public static final int MIN_CLIENT_ID = 1;
	public static final int MAX_CLIENT_ID = 20000;
	private static final int MAX_PREMIUM_CLIENT_ID = 1000;

	private final DonutOrderRecordRepository donutOrderRecordRepository;

	@Autowired
	public DonutOrderService(DonutOrderRecordRepository donutOrderRecordRepository) {
		this.donutOrderRecordRepository = donutOrderRecordRepository;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Optional<DonutOrderClientInfoDTO> addOrder(int clientId, int quantity) {
		Optional<DonutOrderRecord> existingOrderOpt = donutOrderRecordRepository.findByClientId(clientId);

		if (existingOrderOpt.isPresent()) {
			return Optional.empty();
		}

		DonutOrderRecord order = new DonutOrderRecord();
		order.setClientId(clientId);
		order.setQuantity(quantity);
		order.setCreationTimeInMillis(System.currentTimeMillis());
		donutOrderRecordRepository.save(order);

		return getOrderInfo(clientId);
	}

	public Optional<DonutOrderClientInfoDTO> getOrderInfo(int clientId) {
		DonutQueue queue = createQueue();
		return queue.getOrderInfoForClient(clientId);
	}

	public List<DonutOrderManagerInfoDTO> getAllOrderInfos() {
		DonutQueue queue = createQueue();
		return queue.getAllOrderInfos();
	}

	private int compareRecords(DonutOrderRecord a, DonutOrderRecord b) {
		if (a.getClientId() <= MAX_PREMIUM_CLIENT_ID && b.getClientId() > MAX_PREMIUM_CLIENT_ID) {
			return -1;
		}

		if (a.getClientId() > MAX_PREMIUM_CLIENT_ID && b.getClientId() <= MAX_PREMIUM_CLIENT_ID) {
			return 1;
		}

		return Long.compare(a.getCreationTimeInMillis(), b.getCreationTimeInMillis());
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean cancelOrder(int clientId) {
		Optional<DonutOrderRecord> existingOrderOpt = donutOrderRecordRepository.findByClientId(clientId);

		if (existingOrderOpt.isEmpty()) {
			return false;
		}

		donutOrderRecordRepository.delete(existingOrderOpt.get());
		return true;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<DonutOrderCartInfoDTO> retrieveAndUpdateNextOrdersInQueue() {
		DonutQueue queue = createQueue();
		DonutQueueCartResult cartResult = queue.getOrderInfosForNextCart();

		if (!cartResult.toBeDeleted().isEmpty()) {
			donutOrderRecordRepository.deleteAll(cartResult.toBeDeleted());
		}

		if (cartResult.toBeUpdatedOpt().isPresent()) {
			donutOrderRecordRepository.save(cartResult.toBeUpdatedOpt().get());
		}

		return cartResult.dtos();
	}

	private DonutQueue createQueue() {
		List<DonutOrderRecord> allRecords = donutOrderRecordRepository.findAll();
		allRecords.sort(this::compareRecords);
		return new DonutQueue(allRecords);
	}
}