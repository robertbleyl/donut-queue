package robert.bleyl.donutqueue.service;

import java.util.List;
import java.util.Optional;

import robert.bleyl.donutqueue.database.DonutOrderCartInfoDTO;
import robert.bleyl.donutqueue.database.DonutOrderRecord;

public record DonutQueueCartResult(
		List<DonutOrderCartInfoDTO> dtos,
		List<DonutOrderRecord> toBeDeleted,
		Optional<DonutOrderRecord> toBeUpdatedOpt) {

}