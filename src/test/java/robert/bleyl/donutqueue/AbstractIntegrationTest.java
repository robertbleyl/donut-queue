package robert.bleyl.donutqueue;

import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import robert.bleyl.donutqueue.database.DonutOrderRecordRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Testcontainers
public abstract class AbstractIntegrationTest {

	@Container
	PostgreSQLContainer<?> psql = new PostgreSQLContainer<>(PostgreSQLContainer.IMAGE + ":" + PostgreSQLContainer.DEFAULT_TAG)
			.withDatabaseName("testdatabasename")
			.withUsername("testuser")
			.withPassword("testpw");

	@BeforeClass
	static void setupConnection() {
		RestAssured.baseURI = "http://localhost";
		RestAssured.port = 8080;
	}

	@Autowired
	protected DonutOrderRecordRepository donutOrderRecordRepository;

	protected RequestSpecBuilder requestSpecBuilder;

	@BeforeEach
	void setup() {
		requestSpecBuilder = new RequestSpecBuilder();
		requestSpecBuilder.setContentType(ContentType.JSON).addHeader("Accept", ContentType.JSON.getAcceptHeader());
	}

	@AfterEach
	void cleanup() {
		donutOrderRecordRepository.deleteAll();
	}
}
