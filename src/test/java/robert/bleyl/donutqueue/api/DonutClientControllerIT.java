package robert.bleyl.donutqueue.api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import robert.bleyl.donutqueue.AbstractIntegrationTest;
import robert.bleyl.donutqueue.service.DonutOrderClientInfoDTO;
import robert.bleyl.donutqueue.service.DonutQueue;

class DonutClientControllerIT extends AbstractIntegrationTest {

	@Test
	void shouldReturnBadRequestWhenAddingOrderBecauseClientIdInvalid() {
		given().log().all().spec(requestSpecBuilder.build())
				.when().post("/donut-orders/555555?quantity=2")
				.then().statusCode(HttpStatus.BAD_REQUEST.value()).body(equalTo("The clientId '555555' is invalid!"));
	}

	@Test
	void shouldReturnConflictWhenAddingOrderBecauseOrderAlreadyExists() {
		given().log().all().spec(requestSpecBuilder.build())
				.when().post("/donut-orders/2000?quantity=2")
				.then().statusCode(HttpStatus.OK.value());

		given().log().all().spec(requestSpecBuilder.build())
				.when().post("/donut-orders/2000?quantity=1")
				.then().statusCode(HttpStatus.CONFLICT.value());
	}

	@Test
	void shouldReturnNotFoundWhenCancelingOrderInfoBecauseOrderNotPresent() {
		given().log().all().spec(requestSpecBuilder.build())
				.when().delete("/donut-orders/2000")
				.then().statusCode(HttpStatus.NOT_FOUND.value());
	}

	@Test
	void shouldAddAndGetAndCancelOrder() {
		DonutOrderClientInfoDTO clientInfo = given().log().all().spec(requestSpecBuilder.build())
				.when().post("/donut-orders/2000?quantity=2")
				.then().statusCode(HttpStatus.OK.value()).extract().as(DonutOrderClientInfoDTO.class);

		assertEquals(1, clientInfo.getQueuePosition());
		assertTrue(clientInfo.getWaitTimeInMillies() >= DonutQueue.FIVE_MINUTES);
		assertTrue(clientInfo.getWaitTimeInMillies() <= 2 * DonutQueue.FIVE_MINUTES);

		DonutOrderClientInfoDTO newClientInfo = given().log().all().spec(requestSpecBuilder.build())
				.when().get("/donut-orders/2000")
				.then().statusCode(HttpStatus.OK.value()).extract().as(DonutOrderClientInfoDTO.class);

		assertEquals(clientInfo, newClientInfo);

		given().log().all().spec(requestSpecBuilder.build())
				.when().delete("/donut-orders/2000")
				.then().statusCode(HttpStatus.NO_CONTENT.value());

		given().log().all().spec(requestSpecBuilder.build())
				.when().get("/donut-orders/2000")
				.then().statusCode(HttpStatus.NOT_FOUND.value());
	}
}