package robert.bleyl.donutqueue.api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import robert.bleyl.donutqueue.AbstractIntegrationTest;
import robert.bleyl.donutqueue.database.DonutOrderCartInfoDTO;
import robert.bleyl.donutqueue.database.DonutOrderRecord;

class DonutQueueControllerIT extends AbstractIntegrationTest {

	@Test
	void shouldReturnUnauthorizedBecauseNoCredentialsGiven() {
		given().log().all().spec(requestSpecBuilder.build())
				.when().get("/donut-queue")
				.then().statusCode(HttpStatus.UNAUTHORIZED.value());
	}

	@Test
	void shouldReturnUnauthorizedBecauseInvalidCredentialsGiven() {
		given().log().all().spec(requestSpecBuilder.build())
				.auth().basic("jim", "wrongpassword")
				.when().get("/donut-queue")
				.then().statusCode(HttpStatus.UNAUTHORIZED.value());
	}

	@Test
	void shouldReturnEmptyListBecauseNoOrdersPresent() {
		given().log().all().spec(requestSpecBuilder.build())
				.auth().basic("jim", "very$ecureJimPW")
				.when().get("/donut-queue")
				.then().statusCode(HttpStatus.OK.value()).body(equalTo("[]"));
	}

	@Test
	void shouldReturnAllEntriesBecauseCartNotFullYet() {
		DonutOrderRecord order1 = new DonutOrderRecord();
		order1.setClientId(100);
		order1.setQuantity(10);
		order1.setCreationTimeInMillis(System.currentTimeMillis() - 2);

		DonutOrderRecord order2 = new DonutOrderRecord();
		order2.setClientId(200);
		order2.setQuantity(20);
		order2.setCreationTimeInMillis(System.currentTimeMillis() - 1);
		donutOrderRecordRepository.saveAll(List.of(order1, order2));

		DonutOrderCartInfoDTO[] results = given().log().all().spec(requestSpecBuilder.build())
				.auth().basic("jim", "very$ecureJimPW")
				.when().get("/donut-queue")
				.then().statusCode(HttpStatus.OK.value()).extract().as(DonutOrderCartInfoDTO[].class);

		assertEquals(2, results.length);

		DonutOrderCartInfoDTO firstResult = results[0];
		assertEquals(100, firstResult.getClientId());
		assertEquals(10, firstResult.getQuantity());

		DonutOrderCartInfoDTO secondResult = results[1];
		assertEquals(200, secondResult.getClientId());
		assertEquals(20, secondResult.getQuantity());

		assertEquals(0, donutOrderRecordRepository.count());
	}

	@Test
	void shouldReturnOnlyEntriesForFullCart() {
		DonutOrderRecord order1 = new DonutOrderRecord();
		order1.setClientId(100);
		order1.setQuantity(10);
		order1.setCreationTimeInMillis(System.currentTimeMillis() - 3);

		DonutOrderRecord order2 = new DonutOrderRecord();
		order2.setClientId(200);
		order2.setQuantity(20);
		order2.setCreationTimeInMillis(System.currentTimeMillis() - 2);

		DonutOrderRecord order3 = new DonutOrderRecord();
		order3.setClientId(300);
		order3.setQuantity(30);
		order3.setCreationTimeInMillis(System.currentTimeMillis() - 1);

		donutOrderRecordRepository.saveAll(List.of(order1, order2, order3));

		DonutOrderCartInfoDTO[] results = given().log().all().spec(requestSpecBuilder.build())
				.auth().basic("jim", "very$ecureJimPW")
				.when().get("/donut-queue")
				.then().statusCode(HttpStatus.OK.value()).extract().as(DonutOrderCartInfoDTO[].class);

		assertEquals(3, results.length);

		DonutOrderCartInfoDTO firstResult = results[0];
		assertEquals(100, firstResult.getClientId());
		assertEquals(10, firstResult.getQuantity());

		DonutOrderCartInfoDTO secondResult = results[1];
		assertEquals(200, secondResult.getClientId());
		assertEquals(20, secondResult.getQuantity());

		DonutOrderCartInfoDTO thirdResult = results[2];
		assertEquals(300, thirdResult.getClientId());
		assertEquals(20, thirdResult.getQuantity());

		List<DonutOrderRecord> ordersInDb = donutOrderRecordRepository.findAll();
		assertEquals(1, ordersInDb.size());

		DonutOrderRecord orderInDb = ordersInDb.get(0);
		assertEquals(300, orderInDb.getClientId());
		assertEquals(10, orderInDb.getQuantity());
	}
}