package robert.bleyl.donutqueue.api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import robert.bleyl.donutqueue.AbstractIntegrationTest;
import robert.bleyl.donutqueue.database.DonutOrderManagerInfoDTO;
import robert.bleyl.donutqueue.database.DonutOrderRecord;

class DonutManagementControllerIT extends AbstractIntegrationTest {

	@Test
	void shouldReturnUnauthorizedBecauseNoCredentialsGiven() {
		given().log().all().spec(requestSpecBuilder.build())
				.when().get("/donut-management")
				.then().statusCode(HttpStatus.UNAUTHORIZED.value());
	}

	@Test
	void shouldReturnUnauthorizedBecauseInvalidCredentialsGiven() {
		given().log().all().spec(requestSpecBuilder.build())
				.auth().basic("manager", "wrongpassword")
				.when().get("/donut-management")
				.then().statusCode(HttpStatus.UNAUTHORIZED.value());
	}

	@Test
	void shouldReturnEmptyListBecauseNoOrdersPresent() {
		given().log().all().spec(requestSpecBuilder.build())
				.auth().basic("manager", "very$ecureManagerPW")
				.when().get("/donut-management")
				.then().statusCode(HttpStatus.OK.value()).body(equalTo("[]"));
	}

	@Test
	void shouldReturnAllEntries() {
		DonutOrderRecord order1 = new DonutOrderRecord();
		order1.setClientId(1000);
		order1.setQuantity(100);
		order1.setCreationTimeInMillis(System.currentTimeMillis() - 1);

		DonutOrderRecord order2 = new DonutOrderRecord();
		order2.setClientId(2000);
		order2.setQuantity(200);
		order2.setCreationTimeInMillis(System.currentTimeMillis() - 2);
		donutOrderRecordRepository.saveAll(List.of(order1, order2));

		DonutOrderManagerInfoDTO[] results = given().log().all().spec(requestSpecBuilder.build())
				.auth().basic("manager", "very$ecureManagerPW")
				.when().get("/donut-management")
				.then().statusCode(HttpStatus.OK.value()).extract().as(DonutOrderManagerInfoDTO[].class);

		assertEquals(2, results.length);

		DonutOrderManagerInfoDTO firstResult = results[0];
		assertEquals(1000, firstResult.getClientId());
		assertEquals(100, firstResult.getQuantity());

		DonutOrderManagerInfoDTO secondResult = results[1];
		assertEquals(2000, secondResult.getClientId());
		assertEquals(200, secondResult.getQuantity());
	}
}
